# Lucia App

In Germany, the government is rolling out an app for QR code based contact
tracing. The app claims to protect user data but actually it does not, at least
at the time of writing.

The name of that app shall not be named here. It will be referred to in this 
document as the "real" app.

The Lucia app is capable of generating fake QR codes that users can use to
check in at locations while not providing any personal data whatsoever.

You can
[get the app](https://apt.izzysoft.de/fdroid/index/apk/digital.selfdefense.lucia)
on
[F-Droid](https://f-droid.org/)
from the
[IzzyOnDroid repo](https://apt.izzysoft.de/fdroid/).

Alternatively, go to
[Releases](https://codeberg.org/errhammr/Lucia-App/releases) to download the
App (APK).

*A huge thank you to the awesome members of the FLOSS community who made the*
*Lucia app available on F-Droid!*

# How it works

The "real" app has four different ways of checking in at a location:

1. Letting a staff member at the location scan a QR code from your smartphone
2. Scanning a QR code provided by the location using your smartphone
3. Letting a staff member at the location scan a QR code from a special key tag

The Lucia app can help you check in using the first option. It generates and
shows a QR code that can be scanned by a staff member at the location. This QR
code will be considered valid by the check-in system. It does not, however,
contain any personal data about you.

The "real" app puts an encrypted identifier into the QR code that tells the
system who you are. This identifier is encrypted and cannot be decrypted by the
check-in system. The check-in system will simply forward the encrypted
identifier to a server where it will be stored. Some time later (days, maybe
weeks) and only when an infected person has visited the location at the same
time as you, the encrypted identifier will be decrypted.

The Lucia app makes use of the fact that encrypted data is indistinguishable
from random data. So instead of an encrypted identifier it puts random data
into the QR code. This does not interfere with the check-in process because
the check-in system does not decrypt the identifier. So basically, the Lucia
app allows you to enter any location that you would need the "real" app for.

It does not, however, let you perform a check-out procedure. That is not a
problem because you will automatically get checked out after 24 hours. Also
you will not be warned or contacted in the case of a possible infection.

# Why though?

I've created the app mainly out of curiosity. I wanted to see if it was possible
to create fake QR codes that let you complete the check-in procedure at
locations. It turned out to be possible and so I created this app to make it
accessible to every person using an Android smartphone (Version 4.4 "Kitkat" or
newer).

# Future plans

Currently I do not plan to develop this app any further. It does what it was
made for. I expect the "real" app to advance and it is likely that the Lucia
app will stop working some time in the future if the QR code format of the
"real" app changes. I may or may not adjust the Lucia app to be compatible
with any new or other features of the "real" app.

You can, however, fork this repository and tinker with the code. Try, learn and
improve things. Feel free to create pull requests or maintain your own fork.

# License

This app and it's source code are published under the terms of the EUPL version
1.2 only. See LICENSE_EN.txt for an English version of the license.

Diese App und ihr Quellcode werden unter den Bedingungen der EUPL Version 1.2
veröffentlicht. Siehe LICENSE_DE.txt für eine deutsche Fassung der Lizenz.
